const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');

//swagger
var swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');

//HTTPS cert init

var fs = require('fs')
var https = require('https')


//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//Initiate our app
const app = express();


//Configure our app
app.use(cors());
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ name:'session' , secret: 'ashoppingstar', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
app.use(cookieParser());

//swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


if(!isProduction) {
  app.use(errorHandler());
}

//Configure Mongoose
mongoose.connect('mongodb://localhost:27017/softeng');
mongoose.set('debug', true);

//Models & routes
require('./models/Users');
require('./models/Activities');
require('./models/Sportscenters');
require('./models/Blacklists');
require('./models/ActivitySportscenters');
require('./models/Prices');


require('./config/passport');
app.use(require('./routes'));

//error handling 

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(403).send('You are not allowed to do such action');
  }
});

if(!isProduction) {
  app.use((req, res, err) => {
    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.use((req, res,err) => {
  res.status(err.status || 500);

  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});


https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app)
.listen(8765, function () {
  console.log('Server running on https://localhost:8765/')
})


var http = require('http')
http.createServer(app).listen(8766, function() {
    console.log('Express HTTP server listening on port 8765');
});

//app.listen(8000, () => console.log('Server running on http://localhost:8000/'));
